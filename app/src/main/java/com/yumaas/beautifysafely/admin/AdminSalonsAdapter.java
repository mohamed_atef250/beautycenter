package com.yumaas.beautifysafely.admin;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.CustomDialogClass;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.OnItemClickListener;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.SalonDetailsFragment;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;

import java.util.ArrayList;

public class AdminSalonsAdapter extends RecyclerView.Adapter<AdminSalonsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
 
    ArrayList<User> users;
    Context context;

    public AdminSalonsAdapter(Context context , OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener=onItemClickListener;
        this.users = users;
        this.context=context;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public AdminSalonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_barber_makup, parent, false);
        AdminSalonsAdapter.ViewHolder viewHolder = new AdminSalonsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final AdminSalonsAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.imageView,users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.address.setText(users.get(position).address);

        if(users.get(position).accepted==0){
            holder.options.setVisibility(View.VISIBLE);
        }else {
            holder.options.setVisibility(View.GONE);

        }
        try {
            CustomDialogClass alertDialog= new CustomDialogClass((Activity) holder.itemView.getContext());

            holder.delete.setOnClickListener(view -> SweetDialogs.twoButtonDialog(alertDialog,
                    "حذف الصالون"
                    , "هل متاكد من حذف الصالون ؟!",
                    "نعم",
                    "خروج", view1 -> {
                        alertDialog.cancel();
                        alertDialog.dismiss();
                        DataBaseHelper.removeUser(users.get(position));
                        users.remove(position);
                        notifyDataSetChanged();
                        SweetDialogs.singleButtonMessage(holder.itemView.getContext(), "تم المسح بنجاح");
                    }));

            holder.reject.setOnClickListener(view -> SweetDialogs.twoButtonDialog(alertDialog,
                    "حذف الصالون"
                    , "هل متاكد من حذف الصالون ؟!",
                    "نعم",
                    "خروج", view1 -> {
                        alertDialog.cancel();
                        alertDialog.dismiss();
                        DataBaseHelper.removeUser(users.get(position));
                        users.remove(position);
                        notifyDataSetChanged();
                        SweetDialogs.singleButtonMessage(holder.itemView.getContext(), "تم المسح بنجاح");
                    }));

        }catch (Exception e){
            e.getStackTrace();
        }


                   holder.accept.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View view) {
                                                 users.get(position).accepted=1;
                                                 DataBaseHelper.updateUser(users.get(position));
                                                 SweetDialogs.singleButtonMessage(holder.itemView.getContext(), "تم الموافقه بنجاح");

                                             }
                                         });


                holder.itemView.setOnClickListener(view -> {
                    Bundle bundle = new Bundle();
                    SalonDetailsFragment salonDetailsFragment = new SalonDetailsFragment();
                    bundle.putSerializable("salon", users.get(position));
                    salonDetailsFragment.setArguments(bundle);

                    FragmentHelper.addFragment(context, salonDetailsFragment, "SalonDetailsFragment");
                });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,delete;
        TextView name,phone,address;
        Button accept,reject;
        LinearLayout options;
        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            name= view.findViewById(R.id.name);
            phone= view.findViewById(R.id.phone);
            address= view.findViewById(R.id.address);
            delete=view.findViewById(R.id.delete);
            accept=view.findViewById(R.id.accept);
            reject=view.findViewById(R.id.reject);
            options=view.findViewById(R.id.options);

        }
    }
}