package com.yumaas.beautifysafely.admin;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.abedalkareem.tabview.AMTabView;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.salon.SalonMainActivity;

import java.util.ArrayList;
import java.util.List;


public class AdminMainActivity extends AppCompatActivity {
    ImageView imageView;
    private PopupMenu popUp = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.iv_main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        imageView= findViewById(R.id.language);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> genderList = new ArrayList<>();


                genderList.add("اللغه العربيه");
                genderList.add("English");


                if (popUp == null) {
                    popUp = new PopupMenu(AdminMainActivity.this, imageView);
                    for (int i = 0; i < genderList.size(); i++) {
                        popUp.getMenu().add(i, i, i, genderList.get(i));
                    }


                    popUp.setOnMenuItemClickListener(item -> {

                        return false;
                    });
                }
                popUp.show();
                popUp = null;
            }
        });



        AMTabView bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setVisibility(View.GONE);
        FragmentHelper.addFragment(AdminMainActivity.this,new FragmentAdminSalons(),"FragmentHome");
    }
}