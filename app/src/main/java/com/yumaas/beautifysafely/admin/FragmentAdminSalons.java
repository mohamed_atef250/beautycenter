package com.yumaas.beautifysafely.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.BarberAdapter;
import com.yumaas.beautifysafely.OnItemClickListener;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.User;

import java.util.ArrayList;


public class FragmentAdminSalons extends Fragment {
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_halls, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view);

        final ArrayList<User> users = new ArrayList<>();
        final ArrayList<User> usersTemp = DataBaseHelper.getDataLists().users;

        for(int i=0; i<usersTemp.size(); i++){
            if(usersTemp.get(i).type!=1){
                users.add(usersTemp.get(i));
            }
        }



        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        AdminSalonsAdapter categoriesAdapter1 = new AdminSalonsAdapter(getActivity(),new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, users);
        recyclerView.setAdapter(categoriesAdapter1);
        return rootView;
    }


}
