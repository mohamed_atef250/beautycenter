package com.yumaas.beautifysafely;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BarberActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_halls);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        final ArrayList<Integer> images = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            images.add(R.drawable.barber2);
            images.add(R.drawable.barber5);
            images.add(R.drawable.barber3);
            images.add(R.drawable.barber4);
            images.add(R.drawable.barber1);
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        BarberAdapter categoriesAdapter1 = new BarberAdapter(this,new OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {

            }
        }, images);
        recyclerView.setAdapter(categoriesAdapter1);
    }
}