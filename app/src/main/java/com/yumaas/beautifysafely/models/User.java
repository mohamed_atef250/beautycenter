package com.yumaas.beautifysafely.models;


import com.yumaas.beautifysafely.base.DataBaseHelper;
import java.io.Serializable;
import java.util.ArrayList;


public class User implements Serializable {
    public  int id,type;
    public int accepted=0;
   public String address,name,phone,password,image,userName;
   public ArrayList<TimeItem>times;
    public ArrayList<Service>services;
    public  ArrayList<String> images;
    public double lat,lng;


    public User(String name,String phone,String address,String password , String userName,String image){
        this.id= DataBaseHelper.generateId();
        this.name=name;
        this.phone=phone;
        this.address=address;
        this.password=password;
        this.userName=userName;
        this.image=image;
    }



}
