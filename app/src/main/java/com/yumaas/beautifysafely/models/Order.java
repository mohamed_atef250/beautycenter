package com.yumaas.beautifysafely.models;



import com.yumaas.beautifysafely.base.DataBaseHelper;

import java.io.Serializable;

public class Order implements Serializable {

    public int id,quantity=0;
    public User user;
    public Book flower;

    public Order(User user, Book flower, int quantity){
        this.id= DataBaseHelper.generateId();
        this.user=user;
        this.flower=flower;
        this.quantity=quantity;
    }
}
