package com.yumaas.beautifysafely.models;

import com.yumaas.beautifysafely.base.DataBaseHelper;

import java.io.Serializable;

public class UserMeessage implements Serializable {
    public int id;
    public String comment;
    public User user;

    public UserMeessage(String comment, User user){
        this.id= DataBaseHelper.generateId();
        this.comment=comment;
        this.user=user;
    }

}
