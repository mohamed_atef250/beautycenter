package com.yumaas.beautifysafely.models;



import com.yumaas.beautifysafely.base.DataBaseHelper;

import java.io.Serializable;

public class Book implements Serializable {

    public int id;
    public String book,name;

    public Book(String name, String book){
        this.id= DataBaseHelper.generateId();
        this.book=book;
        this.name=name;
    }
}
