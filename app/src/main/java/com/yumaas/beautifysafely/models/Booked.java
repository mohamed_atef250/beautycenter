package com.yumaas.beautifysafely.models;



import com.yumaas.beautifysafely.base.DataBaseHelper;

import java.io.Serializable;

public class Booked implements Serializable {

    public int id;
    public int userId,salonId;
    public TimeItem timeItem;
    public int total=0;
    public String services="";

    public Booked(int userId , int salonId, TimeItem timeItem){
        this.id= DataBaseHelper.generateId();
        this.userId=userId;
        this.salonId=salonId;
        this.timeItem = timeItem;
    }
}
