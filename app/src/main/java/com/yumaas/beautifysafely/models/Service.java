package com.yumaas.beautifysafely.models;



import com.yumaas.beautifysafely.base.DataBaseHelper;

import java.io.Serializable;
import java.util.ArrayList;

public class Service implements Serializable {
    public int id;

    public String title,price;


    public Service(String title, String price){
        this.id= DataBaseHelper.generateId();
        this.title=title;
        this.price=price;

    }

}
