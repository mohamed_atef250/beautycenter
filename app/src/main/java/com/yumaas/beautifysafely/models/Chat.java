package com.yumaas.beautifysafely.models;

import java.io.Serializable;

public class Chat implements Serializable {

    public User user;
    public User teacher;
    public String message;
    public int sender;

    public Chat(User user, User teacher, String message){
        this.user = user;
        this.teacher = teacher;
        this.message=message;
    }

}
