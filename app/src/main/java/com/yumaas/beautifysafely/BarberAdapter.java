package com.yumaas.beautifysafely;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BarberAdapter extends RecyclerView.Adapter<BarberAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<Integer> images;
    Context context;

    public BarberAdapter(Context context , OnItemClickListener onItemClickListener, ArrayList<Integer> images) {
        this.onItemClickListener=onItemClickListener;
        this.images = images;
        this.context=context;
    }


    @Override
    public int getItemCount() {
        return 40;
    }


    @Override
    public BarberAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_barber_makup, parent, false);
        BarberAdapter.ViewHolder viewHolder = new BarberAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final BarberAdapter.ViewHolder holder, final int position) {
        holder.imageView.setImageResource(images.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //context.startActivity(new Intent(context,BarberDetailsActivity.class));

                FragmentHelper.addFragment(context,new SalonDetailsFragment(),"SalonDetailsFragment");
            }
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);

        }
    }
}