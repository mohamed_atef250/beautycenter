package com.yumaas.beautifysafely;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.yumaas.beautifysafely.admin.AdminMainActivity;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.salon.SalonMainActivity;
import com.yumaas.beautifysafely.user.MainActivity;


public class LoginActivity extends AppCompatActivity {
    EditText username,password;

    Button btn;
    TextView forgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        forgotPassword = findViewById(R.id.forgot_password);
        btn = findViewById(R.id.button2);

        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.MANAGE_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},34);

        btn.setOnClickListener(view -> {

            if (username.getText().toString().contains("admin")) {
                startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
            }
            else {

                User user = DataBaseHelper.loginUser(username.getText().toString(),password.getText().toString());

                if(user==null){
                    SweetDialogs.singleButtonMessage(LoginActivity.this,"بيانات غير صحيحه");
                }
                else {
                    DataBaseHelper.saveStudent(user);

                    if (user.type == 1) {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));

                    } else {
                        startActivity(new Intent(LoginActivity.this, SalonMainActivity.class));

                    }
                }
            }

        });

        findViewById(R.id.textView).setOnClickListener(view -> {
            userTypesDialog();
        });

        forgotPassword.setOnClickListener(view -> getPassword());

    }

    private void userTypesDialog(){

        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
        builder1.setMessage("من فضللك اختر نوع الحساب");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "صالون",
                (dialog, id) -> {
                    startActivity(new Intent(LoginActivity.this, RegisterSalonActivity.class));
                    dialog.cancel();
                });

        builder1.setNegativeButton(
                "مستخدم",
                (dialog, id) -> {
                    startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

                    dialog.cancel();
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    private void getPassword() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);
        alertDialog.setTitle("استرجاع كلمه المرور");
        alertDialog.setMessage("ادخل اسم المستخدم او البريد وسيتم ارسال الرقم السري في رساله");

        final EditText input = new EditText(LoginActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("استرجاع",
                (dialog, which) -> {
                    User user = DataBaseHelper.findUser(input.getText().toString());
                    if (user == null) {
                        Toast.makeText(LoginActivity.this, "ناسف هذا الحساب غير مسجل من قبل", Toast.LENGTH_SHORT).show();
                    } else {
                        sendNotification(user.password);
                    }
                });

        alertDialog.setNegativeButton("غلق",
                (dialog, which) -> dialog.cancel());

        alertDialog.show();


    }

    private void sendNotification(String password) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "channelId";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("تم استعاده كلمه المرور")
                        .setContentText("كلمه المرور هي "+password)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0, notificationBuilder.build());
    }

}