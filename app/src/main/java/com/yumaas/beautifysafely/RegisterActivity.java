package com.yumaas.beautifysafely;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.base.Validate;
import com.yumaas.beautifysafely.base.filesutils.FileOperations;
import com.yumaas.beautifysafely.base.filesutils.VolleyFileObject;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.salon.SalonMainActivity;
import com.yumaas.beautifysafely.user.MainActivity;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;
import com.yumaas.beautifysafely.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;


public class RegisterActivity extends AppCompatActivity {
    EditText  name,phone,password,image,userName;
    Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        userName = findViewById(R.id.user_name);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        image=findViewById(R.id.image);


        register = findViewById(R.id.register);


        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(view -> {
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });



        register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(name.getText().toString(),
                        phone.getText().toString(),
                       "",
                        password.getText().toString()
                        , userName.getText().toString(),
                        selectedImage
                );
                user.type=1;


                DataBaseHelper.addUser(user);
                DataBaseHelper.saveStudent(user);

                CustomDialogClass alertDialog= new CustomDialogClass(RegisterActivity.this);
                SweetDialogs.successMessage(alertDialog, "تم التسجيل بنجاح", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);

                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }



    ArrayList<VolleyFileObject> volleyFileObjects;
    String selectedImage;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            try {
                volleyFileObjects = new ArrayList<>();
                VolleyFileObject volleyFileObject =
                        FileOperations.getVolleyFileObject(RegisterActivity.this, data, "image",
                                43);
                volleyFileObjects.add(volleyFileObject);
                image.setText("تم اختيار الصوره");
                addServiceApi();
            } catch (Exception E) {
                E.getStackTrace();
            }


    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }





    private boolean validate(){

        if(Validate.isEmpty(name.getText().toString())){
            name.setError("ادخل الاسم");
            return false;
        } else if(Validate.isEmpty(phone.getText().toString())){
            phone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(password.getText().toString())){
            password.setError("ادخل كلمه المرور");
            return false;
        }

        return true;
    }
}