package com.yumaas.beautifysafely;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.base.PopUpItem;
import com.yumaas.beautifysafely.base.PopUpMenus;
import com.yumaas.beautifysafely.base.Validate;
import com.yumaas.beautifysafely.base.filesutils.FileOperations;
import com.yumaas.beautifysafely.base.filesutils.VolleyFileObject;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.salon.SalonMainActivity;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;
import com.yumaas.beautifysafely.volleyutils.ConnectionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RegisterSalonActivity extends AppCompatActivity {
    EditText address,name,phone,password,image,userName,location;
    Button register;
    double lat,lng;
    String selectedImage;

    private PopupMenu popUp = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register_salons);

        address = findViewById(R.id.address);
        userName = findViewById(R.id.user_name);
        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        location = findViewById(R.id.location);
        password = findViewById(R.id.password);
        image=findViewById(R.id.image);



        register = findViewById(R.id.register);


        address.setFocusable(false);
        address.setClickable(true);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> genderList = new ArrayList<>();


                genderList.add("الخالدية");
                genderList.add("الفيصلية");


                genderList.add("المحمدية");
                genderList.add("الصفاء");
                genderList.add("الفيحاء");
                genderList.add("الواحة");
                genderList.add("التلال");
                genderList.add("الربوة");
                genderList.add("العزيزية");
                genderList.add("الندوة");
                genderList.add("السليمانية");
                genderList.add("ابو موسى الاشعري");
                genderList.add("النايفية");
                genderList.add("النهضة");
                genderList.add("الورود");
                genderList.add("البلدية");


                if (popUp == null) {
                    popUp = new PopupMenu(RegisterSalonActivity.this, address);
                    for (int i = 0; i < genderList.size(); i++) {
                        popUp.getMenu().add(i, i, i, genderList.get(i));
                    }
                    popUp.setOnMenuItemClickListener(item -> {
                        address.setText(item.getTitle());
                        return false;
                    });
                }
                popUp.show();
                popUp = null;
            }
        });



        location.setFocusable(false);
        location.setClickable(true);
        location.setOnClickListener(view -> {
            Intent intent = new Intent(this, SelectLocationActivity.class);
            startActivityForResult(intent, 150);
        });



        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(view -> {

            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });



        register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(name.getText().toString(),
                        phone.getText().toString(),
                        address.getText().toString(),
                        password.getText().toString()
                        , userName.getText().toString(),
                        selectedImage
                       );
                user.type=2;
                user.lat=lat;
                user.lng=lng;
                DataBaseHelper.addUser(user);
                DataBaseHelper.saveStudent(user);

                CustomDialogClass alertDialog= new CustomDialogClass(RegisterSalonActivity.this);
                SweetDialogs.successMessage(alertDialog, "تم التسجيل بنجاح", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(RegisterSalonActivity.this, SalonMainActivity.class);

                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }



    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==150){
            location.setText("تم اختيار الموقع بنجاح");
            lat=data.getDoubleExtra("lat",0.0);
            lng=data.getDoubleExtra("lng",0.0);

        }else {
            try {
                volleyFileObjects = new ArrayList<>();
                VolleyFileObject volleyFileObject =
                        FileOperations.getVolleyFileObject(RegisterSalonActivity.this, data, "image",
                                43);
                volleyFileObjects.add(volleyFileObject);
                image.setText("تم اختيار الصوره");
                addServiceApi();
            } catch (Exception E) {
                E.getStackTrace();
            }
        }

    }


    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(RegisterSalonActivity.this);
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }





    private boolean validate(){

        if(Validate.isEmpty(name.getText().toString())){
            name.setError("ادخل الاسم");
            return false;
        } else if(Validate.isEmpty(phone.getText().toString())){
            phone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(password.getText().toString())){
            password.setError("ادخل كلمه المرور");
            return false;
        }

        return true;
    }


}
