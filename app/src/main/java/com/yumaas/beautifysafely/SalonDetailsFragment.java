package com.yumaas.beautifysafely;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.salon.SalonServicesAdapter;
import com.yumaas.beautifysafely.salon.TimesAdapter;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;

import java.util.ArrayList;


public class SalonDetailsFragment extends Fragment {
    View rootView;
    SliderView sliderView;
    RecyclerView menuList,menuList2;
    TextView name,phone,address;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_barber_makeup_details, container, false);

        rootView.findViewById(R.id.button2).setVisibility(View.GONE);
        User user = (User) getArguments().getSerializable("salon");

        name = rootView.findViewById(R.id.name);
        phone = rootView.findViewById(R.id.phone);
        address = rootView.findViewById(R.id.address);


        name.setText(user.name);
        phone.setText(user.phone);
        address.setText(user.address);

        setSliderView(user.images);
        menuList = rootView.findViewById(R.id.menu_list);
        TimesAdapter chatAdapter = new TimesAdapter(getActivity(),user.times);
        menuList.setAdapter(chatAdapter);



        menuList2 = rootView.findViewById(R.id.menu_list2);
        menuList2.setLayoutManager(new GridLayoutManager(getActivity(),2));
        SalonServicesAdapter salonServicesAdapter = new SalonServicesAdapter(getActivity(), user.services);
        menuList2.setAdapter(salonServicesAdapter);

        return rootView;
    }


    private void setSliderView( ArrayList<String> images){

        sliderView =  rootView.findViewById(R.id.imageSlider);
        sliderView.setSliderAdapter(new SliderAdapterBarber(getActivity(),images));
        sliderView.startAutoCycle();
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.THIN_WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.FANTRANSFORMATION);
        sliderView.setScrollTimeInSec(4);
    }



}
