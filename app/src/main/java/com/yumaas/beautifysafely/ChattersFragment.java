package com.yumaas.beautifysafely;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


public class ChattersFragment extends Fragment {
    View rootView;
    RecyclerView menuList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);


        menuList = rootView.findViewById(R.id.menu_list);
        ChattersAdapter chatAdapter = new ChattersAdapter(getActivity());
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
