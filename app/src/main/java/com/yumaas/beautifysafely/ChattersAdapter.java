package com.yumaas.beautifysafely;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


public class ChattersAdapter extends RecyclerView.Adapter<ChattersAdapter.ViewHolder> {

    Context context;

    public ChattersAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        ChattersAdapter.ViewHolder viewHolder = new ChattersAdapter.ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChattersAdapter.ViewHolder holder, final int position) {

        //holder.message.setText(chats.get(position).admin.name);

//        if (position % 2 == 0) {
//            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
//            // ConnectionHelper.loadImage(holder.image, chats.get(position).user.image);
//        } else {
//            holder.chatterLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
//            // ConnectionHelper.loadImage(holder.image, chats.get(position).admin.image);
//
//        }
        holder.itemView.setOnClickListener(view -> {
            ChatFragment chatFragment = new ChatFragment();

            FragmentHelper.replaceFragment(holder.itemView.getContext(), chatFragment, "ChatFragment");
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
