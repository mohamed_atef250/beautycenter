package com.yumaas.beautifysafely.base;

import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;

import java.util.ArrayList;


public class PopUpMenus {

    public static PopupMenu showPopUp(Context context, View view, ArrayList<PopUpItem> types) {
        PopupMenu typesPopUps;
        typesPopUps = new PopupMenu(context, view);

        for (int i = 0; i < types.size(); i++) {
            typesPopUps.getMenu().add(types.get(i).id, types.get(i).id, types.get(i).id, types.get(i).name);
        }
        typesPopUps.show();

        return typesPopUps;

    }


}