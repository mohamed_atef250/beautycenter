package com.yumaas.beautifysafely.base;

import com.yumaas.beautifysafely.models.Ads;
import com.yumaas.beautifysafely.models.Book;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.Chat;
import com.yumaas.beautifysafely.models.Order;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.models.UserMeessage;

import java.util.ArrayList;


public class DataLists {

    public ArrayList<User>  users = new ArrayList<>();
    public ArrayList<Book>  books = new ArrayList<>();
    public ArrayList<Booked>  bookeds = new ArrayList<>();
    public ArrayList<UserMeessage>userMeessages = new ArrayList<>();
    public ArrayList<Chat>  chats = new ArrayList<>();
    public ArrayList<Ads>   ads = new ArrayList<>();
    public ArrayList<Book>  flowers = new ArrayList<>();
    public ArrayList<Order> orders = new ArrayList<>();

}
