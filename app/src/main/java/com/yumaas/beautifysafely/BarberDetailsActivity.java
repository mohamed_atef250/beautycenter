package com.yumaas.beautifysafely;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class BarberDetailsActivity extends AppCompatActivity {

    SliderView sliderView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_barber_makeup_details);

         sliderView = findViewById(R.id.imageSlider);
        sliderView.setSliderAdapter(new SliderAdapterBarber(this,new ArrayList<>()));
        sliderView.startAutoCycle();
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.THIN_WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.FANTRANSFORMATION);
        sliderView.setScrollTimeInSec(4);


    }


}
