package com.yumaas.beautifysafely;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
