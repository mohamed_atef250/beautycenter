package com.yumaas.beautifysafely;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;




public class ChatFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    private EditText message;
    private Button send;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        message = rootView.findViewById(R.id.et_login_first_name);
        send = rootView.findViewById(R.id.btn_login_sign_in);
        menuList = rootView.findViewById(R.id.menu_list);


        ChatAdapter chatAdapter = new ChatAdapter(getActivity());
        menuList.setAdapter(chatAdapter);



        return rootView;
    }


}
