package com.yumaas.beautifysafely;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;

import java.util.ArrayList;

public class SliderAdapterBarber extends SliderViewAdapter<SliderAdapterBarber.SliderAdapterVH> {

    private Context context;
    ArrayList<String> images;
    public SliderAdapterBarber(Context context, ArrayList<String> images) {
        this.images=images;
        this.context = context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hall_image, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {


        ConnectionHelper.loadImage(viewHolder.imageView,images.get(position));

    }

    @Override
    public int getCount() {
        return images==null?0:images.size();
    }

    public static  class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        ImageView imageView;

        public SliderAdapterVH(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);

        }
    }
}