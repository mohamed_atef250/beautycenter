package com.yumaas.beautifysafely.salon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.User;

import java.util.ArrayList;

public class SalonBookedTimesAdapter extends RecyclerView.Adapter<SalonBookedTimesAdapter.ViewHolder> {


    ArrayList<Booked>timeItems;
    Context context;

    public SalonBookedTimesAdapter(Context context , ArrayList<Booked>timeItems) {
        this.context=context;
        this.timeItems=timeItems;

    }


    @Override
    public int getItemCount() {
        return timeItems==null?0:timeItems.size();
    }


    @Override
    public SalonBookedTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time
                , parent, false);
        SalonBookedTimesAdapter.ViewHolder viewHolder = new SalonBookedTimesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SalonBookedTimesAdapter.ViewHolder holder, final int position) {

        User user = DataBaseHelper.findUser(timeItems.get(position).userId);
        holder.time.setText(" معاد الحجز : "+timeItems.get(position).timeItem.time+"  \n\n "+" اسم العميل  :  "+
                user.name+"  \n\n "+" رقم الهاتف  : "+
                user.phone+"  \n\n "+" الخدمات المختارة  : "+timeItems.get(position).services);


    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        public ViewHolder(View view) {
            super(view);
        time = view.findViewById(R.id.tv_review_reviewer);

        }
    }






}