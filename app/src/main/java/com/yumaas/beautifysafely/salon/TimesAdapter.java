package com.yumaas.beautifysafely.salon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.models.TimeItem;

import java.util.ArrayList;

public class TimesAdapter extends RecyclerView.Adapter<TimesAdapter.ViewHolder> {


    ArrayList<TimeItem>timeItems;
    Context context;

    public TimesAdapter(Context context , ArrayList<TimeItem>timeItems ) {
        this.context=context;
        this.timeItems=timeItems;
    }


    @Override
    public int getItemCount() {
        return timeItems==null?0:timeItems.size();
    }


    @Override
    public TimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time
                , parent, false);
        TimesAdapter.ViewHolder viewHolder = new TimesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final TimesAdapter.ViewHolder holder, final int position) {
        holder.time.setText(timeItems.get(position).time);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        public ViewHolder(View view) {
            super(view);
        time = view.findViewById(R.id.tv_review_reviewer);

        }
    }
}