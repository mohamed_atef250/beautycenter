package com.yumaas.beautifysafely.salon;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Chat;
import com.yumaas.beautifysafely.models.User;
import java.util.ArrayList;




public class SalonChatFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    ArrayList<Chat> chatters, chats;
    EditText nameEditText;
    Button chatBtn;
    User teacher,user;
    SalonChatAdapter chatAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        nameEditText = rootView.findViewById(R.id.et_chat_message);
        chatBtn = rootView.findViewById(R.id.btn_chat);


        chats = new ArrayList<>();
        chatters = DataBaseHelper.getDataLists().chats;



        Chat chat = (Chat) getArguments().getSerializable("chatItem");
        user  = chat.user;
        teacher  = DataBaseHelper.getSavedUser();

        

        for (int i = 0; i < chatters.size(); i++) {
            try {
                if (chatters.get(i).teacher.id == teacher.id  && chatters.get(i).user.id == user.id) {
                    chats.add(chatters.get(i));
                }
            }catch (Exception e){
                e.getStackTrace();
            }
        }
        
         chatAdapter = new SalonChatAdapter(getActivity(), chats);
        menuList.setAdapter(chatAdapter);


        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Chat chat = new Chat(user,teacher,nameEditText.getText().toString());
                chat.sender=teacher.id;
                chats.add(chat);
                DataBaseHelper.addChat(chat);
                chatAdapter.notifyDataSetChanged();
                nameEditText.setText("");
            }
        });

        return rootView;
    }


}
