package com.yumaas.beautifysafely.salon;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Chat;
import com.yumaas.beautifysafely.models.User;
import java.util.ArrayList;
import java.util.HashMap;


public class SalonChattersFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    HashMap<String,Boolean  >hashMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        hashMap = new HashMap<>();

        ArrayList<Chat> chatters = new ArrayList<>();

        ArrayList<Chat> chattersTemp = DataBaseHelper.getDataLists().chats;
        User user = DataBaseHelper.getSavedUser();


        for(int i=0; i<chattersTemp.size(); i++){

            if(user.id == chattersTemp.get(i).user.id ||
                    (user.id == chattersTemp.get(i).teacher.id)){
                String text = Math.max(chattersTemp.get(i).user.id,chattersTemp.get(i).teacher.id)+""+
                Math.min(chattersTemp.get(i).teacher.id,chattersTemp.get(i).user.id);

                if(!hashMap.containsKey(text)){
                    chatters.add(chattersTemp.get(i));
                }

                hashMap.put(text,true);
            }

        }


        SalonChattersAdapter chatAdapter = new SalonChattersAdapter(getActivity(),chatters);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
