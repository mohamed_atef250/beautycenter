package com.yumaas.beautifysafely.salon;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.yumaas.beautifysafely.ChattersAdapter;
import com.yumaas.beautifysafely.CustomDialogClass;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.ImageResponse;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.RegisterSalonActivity;
import com.yumaas.beautifysafely.SelectLocationActivity;
import com.yumaas.beautifysafely.SliderAdapterBarber;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.base.filesutils.FileOperations;
import com.yumaas.beautifysafely.base.filesutils.VolleyFileObject;
import com.yumaas.beautifysafely.models.TimeItem;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;
import com.yumaas.beautifysafely.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class UpdateProfileFragment extends Fragment {
    View rootView;
    EditText address,name,phone,password,image,userName,location;
    Button update,addImage;
    SliderView sliderView;
    double lat,lng;
    boolean fromButton;
    private PopupMenu popUp = null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_center_profile, container, false);

        address = rootView.findViewById(R.id.address);
        userName =  rootView.findViewById(R.id.user_name);
        name = rootView.findViewById(R.id.name);
        phone = rootView.findViewById(R.id.phone);
        password = rootView.findViewById(R.id.password);
        image=rootView.findViewById(R.id.image);
        location = rootView.findViewById(R.id.location);
        addImage = rootView.findViewById(R.id.add_image);

        setDetails();
        update = rootView.findViewById(R.id.register);



        location.setFocusable(false);
        location.setClickable(true);
        location.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), SelectLocationActivity.class);
            startActivityForResult(intent, 150);
        });



        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(view -> {
            fromButton=false;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });

        addImage.setOnClickListener(view -> {
            fromButton=true;
            Intent intent;
            intent = new Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
        });


        address.setFocusable(false);
        address.setClickable(true);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> genderList = new ArrayList<>();
                genderList.add("الخالدية");
                genderList.add("الفيصلية");
                genderList.add("المحمدية");
                genderList.add("الصفاء");
                genderList.add("الفيحاء");
                genderList.add("الواحة");
                genderList.add("التلال");
                genderList.add("الربوة");
                genderList.add("العزيزية");
                genderList.add("الندوة");
                genderList.add("السليمانية");
                genderList.add("ابو موسى الاشعري");
                genderList.add("النايفية");
                genderList.add("النهضة");
                genderList.add("الورود");
                genderList.add("البلدية");


                if (popUp == null) {
                    popUp = new PopupMenu(getActivity(), address);
                    for (int i = 0; i < genderList.size(); i++) {
                        popUp.getMenu().add(i, i, i, genderList.get(i));
                    }
                    popUp.setOnMenuItemClickListener(item -> {
                        address.setText(item.getTitle());
                        return false;
                    });
                }
                popUp.show();
                popUp = null;
            }
        });




        update.setOnClickListener(view -> {
            User user = DataBaseHelper.getSavedUser();
            user.name = name.getText().toString();
            user.phone = phone.getText().toString();
            user.address = address.getText().toString();
            user.password = password.getText().toString();
            user.userName = userName.getText().toString();
            user.image=selectedImage;
            user.type=2;
            user.lat=lat;
            user.lng=lng;
            DataBaseHelper.updateUser(user);
            DataBaseHelper.saveStudent(user);

            CustomDialogClass alertDialog= new CustomDialogClass(getActivity());
            SweetDialogs.successMessage(alertDialog, "تم التعديل بنجاح", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.cancel();
                    alertDialog.dismiss();
                }
            });
        });


        return rootView;
    }



    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==150){
            location.setText("تم اختيار الموقع بنجاح");
            lat=data.getDoubleExtra("lat",0.0);
            lng=data.getDoubleExtra("lng",0.0);

        }else {
            try {
                volleyFileObjects = new ArrayList<>();
                VolleyFileObject volleyFileObject =
                        FileOperations.getVolleyFileObject(getActivity(), data, "image",
                                43);
                volleyFileObjects.add(volleyFileObject);
                image.setText("تم اختيار الصوره");
                addServiceApi();
            } catch (Exception E) {
                E.getStackTrace();
            }
        }

    }

    String selectedImage;
    private void addServiceApi() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري تحميل الصوره");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                if(fromButton){

                    User user = DataBaseHelper.getSavedUser();

                    if(user.images==null)user.images=new ArrayList<>();

                    user.images.add(selectedImage);

                    DataBaseHelper.saveStudent(user);
                    DataBaseHelper.updateUser(user);

                    setSliderView(user.images);
                }

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }



    private void setDetails(){

        User user = DataBaseHelper.getSavedUser();
        userName.setText(user.userName);
        name.setText(user.name);
        phone.setText(user.phone);
        address.setText(user.address);
        password.setText(user.password);
        image.setText("تغير الصوره");
        selectedImage=user.image;
        location.setText("تغير الموقع");
        user.lng=lng;
        user.lat=lat;
        setSliderView(user.images);

    }

    private void setSliderView( ArrayList<String> images){

        sliderView =  rootView.findViewById(R.id.imageSlider);
        sliderView.setSliderAdapter(new SliderAdapterBarber(getActivity(),images));
        sliderView.startAutoCycle();
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setIndicatorAnimation(IndicatorAnimationType.THIN_WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.FANTRANSFORMATION);
        sliderView.setScrollTimeInSec(4);
    }


}
