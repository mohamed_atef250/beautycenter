package com.yumaas.beautifysafely.salon;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;


public class SalonTimesFragment extends Fragment {
    View rootView;
    RecyclerView menuList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);

        rootView.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new AddTimesFragment(),"AddTimesFragment");
            }
        });

        menuList = rootView.findViewById(R.id.recycler_view);
        TimesAdapter chatAdapter = new TimesAdapter(getActivity(), DataBaseHelper.getSavedUser().times);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
