package com.yumaas.beautifysafely.salon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.models.Service;
import java.util.ArrayList;

public class SalonServicesAdapter extends RecyclerView.Adapter<SalonServicesAdapter.ViewHolder> {

    ArrayList<Service>services;
    Context context;

    public SalonServicesAdapter(Context context ,ArrayList<Service>services) {
        this.context=context;
        this.services=services;
    }


    @Override
    public int getItemCount() {
        return services==null?0:services.size();
    }


    @Override
    public SalonServicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service
                , parent, false);
        SalonServicesAdapter.ViewHolder viewHolder = new SalonServicesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final SalonServicesAdapter.ViewHolder holder, final int position) {
        holder.time.setText(services.get(position).title+"   "+services.get(position).price+" ريال ");
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView time;
        public ViewHolder(View view) {
            super(view);
        time = view.findViewById(R.id.tv_review_reviewer);

        }
    }
}