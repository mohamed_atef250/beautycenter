package com.yumaas.beautifysafely.salon;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.user.BookedTimesAdapter;

import java.util.ArrayList;


public class SalonBookedTimesFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);
        rootView.findViewById(R.id.button2).setVisibility(View.GONE);

        ArrayList<Booked> bookeds = new ArrayList<>();
        ArrayList<Booked> bookedsTemp = DataBaseHelper.getDataLists().bookeds;

        User user = DataBaseHelper.getSavedUser();

        for(int i=0; i<bookedsTemp.size(); i++){
            if(bookedsTemp.get(i).salonId==user.id){

                bookeds.add(bookedsTemp.get(i));
            }
        }

        SalonBookedTimesAdapter chatAdapter = new SalonBookedTimesAdapter(getActivity(),bookeds);

        menuList = rootView.findViewById(R.id.recycler_view);
        menuList.setLayoutManager(new LinearLayoutManager(getActivity()));
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
