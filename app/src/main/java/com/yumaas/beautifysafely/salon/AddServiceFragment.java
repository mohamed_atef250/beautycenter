package com.yumaas.beautifysafely.salon;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import com.yumaas.beautifysafely.CustomDialogClass;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Service;
import com.yumaas.beautifysafely.models.User;

import java.util.ArrayList;


public class AddServiceFragment extends Fragment {
    View rootView;

    EditText title,price;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_service, container, false);

        title = rootView.findViewById(R.id.title);
        price = rootView.findViewById(R.id.price);

        rootView.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                User user = DataBaseHelper.getSavedUser();

                if(user.services==null)user.services=new ArrayList<>();

                user.services.add(new Service(title.getText().toString(),price.getText().toString()));
                DataBaseHelper.saveStudent(user);
                DataBaseHelper.updateUser(user);
                CustomDialogClass alertDialog= new CustomDialogClass(getActivity());
                SweetDialogs.successMessage(alertDialog,
                        "تم الاضافه بنجاح", view1 -> {
                            alertDialog.cancel();
                            alertDialog.dismiss();

                            FragmentHelper.popAllFragments(getActivity());
                            FragmentHelper.addFragment(getActivity(),new SalonServicesFragment() , "SalonServicesFragment");
                        });
            }
        });


        return rootView;
    }


}
