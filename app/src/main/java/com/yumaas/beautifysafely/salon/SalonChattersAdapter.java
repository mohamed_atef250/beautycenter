package com.yumaas.beautifysafely.salon;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.models.Chat;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;
import java.util.ArrayList;


public class SalonChattersAdapter extends RecyclerView.Adapter<SalonChattersAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat>chattersList;


    public SalonChattersAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList=chattersList;
    }


    @Override
    public SalonChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        SalonChattersAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SalonChattersAdapter.ViewHolder holder, final int position) {

        holder.message.setText(chattersList.get(position).user.name);


       ConnectionHelper.loadImage(holder.image , chattersList.get(position).user.image);


        holder.itemView.setOnClickListener(view -> {

            SalonChatFragment userChatFragment =   new SalonChatFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chattersList.get(position));
            userChatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(), userChatFragment, "ChatFragment");
        });

    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
