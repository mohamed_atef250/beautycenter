package com.yumaas.beautifysafely.salon;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import com.yumaas.beautifysafely.CustomDialogClass;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.TimeItem;
import com.yumaas.beautifysafely.models.User;
import java.util.ArrayList;
import java.util.Calendar;


public class AddTimesFragment extends Fragment {


    View v;

    EditText time;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_add_time, container, false);


        time = v.findViewById(R.id.time);


        selectTimeDialog(time);



        v.findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                User user = DataBaseHelper.getSavedUser();

                if(user.times==null)user.times=new ArrayList<>();

                user.times.add(new TimeItem( time.getText().toString()));
                DataBaseHelper.saveStudent(user);
                DataBaseHelper.updateUser(user);
                CustomDialogClass alertDialog= new CustomDialogClass(getActivity());
                SweetDialogs.successMessage(alertDialog,
                        "تم الاضافه بنجاح", view1 -> {
                            alertDialog.cancel();
                            alertDialog.dismiss();

                            FragmentHelper.popAllFragments(getActivity());
                            FragmentHelper.addFragment(getActivity(),new SalonTimesFragment() , "TimesFragment");
                        });
            }
        });

        return v;
    }

    int mHour, mMinute;

    private void selectTimeDialog(EditText txtTime) {

        txtTime.setFocusable(false);
        txtTime.setClickable(true);

        txtTime.setOnClickListener((View.OnClickListener) view -> {

            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), (timePicker, hourOfDay, i1) -> {

                String text = ((hourOfDay % 12) < 10 ? "0" : "") + "" + (hourOfDay % 12) + ":00";
                if (hourOfDay >= 12) {
                    text += " ص ";
                } else {
                    text += " م ";
                }

                txtTime.setText(text);


            }, mHour, mMinute, false);
            timePickerDialog.show();
        });

    }


}