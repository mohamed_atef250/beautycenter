package com.yumaas.beautifysafely.salon;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import androidx.appcompat.app.AppCompatActivity;

import com.abedalkareem.tabview.AMTabView;
import com.yumaas.beautifysafely.ChattersFragment;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;

import java.util.ArrayList;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;


public class SalonMainActivity extends AppCompatActivity {
    private PopupMenu popUp = null;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AMTabView bottomNavigation = findViewById(R.id.bottom_navigation);

        findViewById(R.id.iv_main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

       imageView= findViewById(R.id.language);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> genderList = new ArrayList<>();


                genderList.add("اللغه العربيه");
                genderList.add("English");


                if (popUp == null) {
                    popUp = new PopupMenu(SalonMainActivity.this, imageView);
                    for (int i = 0; i < genderList.size(); i++) {
                        popUp.getMenu().add(i, i, i, genderList.get(i));
                    }


                    popUp.setOnMenuItemClickListener(item -> {

                        return false;
                    });
                }
                popUp.show();
                popUp = null;
            }
        });


        FragmentHelper.addFragment(SalonMainActivity.this,new SalonTimesFragment(),"TimesFragment");


        List<Integer> icons = new ArrayList<>();

        icons.add(R.drawable.ic_home);
        icons.add(R.drawable.ic_chat);
        icons.add(R.drawable.ic_service);
        icons.add(R.drawable.ic_profile);
        icons.add(R.drawable.ic_times);


        bottomNavigation.setTabsImages(icons);


        bottomNavigation.setOnTabChangeListener(new Function1<Integer, Unit>() {
            @Override
            public Unit invoke(Integer integer) {
                if(integer==0){
                    FragmentHelper.addFragment(SalonMainActivity.this,new SalonTimesFragment(),"FragmentHome");
                }else  if(integer==1){
                    FragmentHelper.addFragment(SalonMainActivity.this,new SalonChattersFragment(),"SalonChattersFragment");
                }else  if(integer==2){
                    FragmentHelper.addFragment(SalonMainActivity.this,new SalonServicesFragment(),"SalonServicesFragment");
                }else  if(integer==3){
                    FragmentHelper.addFragment(SalonMainActivity.this,new UpdateProfileFragment(),"SearchFragment");
                }else {

                    FragmentHelper.addFragment(SalonMainActivity.this,new SalonBookedTimesFragment(),"SearchFragment");


                }
                return null;
            }
        });




    }
}