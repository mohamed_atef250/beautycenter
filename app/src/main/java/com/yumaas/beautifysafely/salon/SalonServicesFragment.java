package com.yumaas.beautifysafely.salon;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;


public class SalonServicesFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    Button button2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);
        button2 =  rootView.findViewById(R.id.button2);
        button2.setText("اضافه خدمه");

        button2.setOnClickListener(view -> FragmentHelper.addFragment(getActivity(),new AddServiceFragment(),"AddServiceFragment"));

        menuList = rootView.findViewById(R.id.recycler_view);
        menuList.setLayoutManager(new GridLayoutManager(getActivity(),2));
        SalonServicesAdapter chatAdapter = new SalonServicesAdapter(getActivity(), DataBaseHelper.getSavedUser().services);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
