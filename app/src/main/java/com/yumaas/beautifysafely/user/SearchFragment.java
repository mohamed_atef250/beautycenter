package com.yumaas.beautifysafely.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;

import java.util.ArrayList;


public class SearchFragment extends Fragment {
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_search, container, false);
        EditText editText=rootView.findViewById(R.id.et_login_first_name);
        rootView.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("query",editText.getText().toString());
                FragmentSearchSalonResult fragmentSearchSalonResult = new FragmentSearchSalonResult();
                fragmentSearchSalonResult.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),fragmentSearchSalonResult,"fragmentSearchSalonResult");
            }
        });
        return rootView;
    }


}
