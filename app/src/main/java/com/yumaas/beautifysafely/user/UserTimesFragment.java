package com.yumaas.beautifysafely.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Book;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.salon.AddTimesFragment;
import com.yumaas.beautifysafely.salon.TimesAdapter;


public class UserTimesFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);
        rootView.findViewById(R.id.button2).setVisibility(View.GONE);


        User user = (User) getArguments().getSerializable("salon");
        UserTimesAdapter chatAdapter = new UserTimesAdapter(getActivity(), user.times,user);

        menuList = rootView.findViewById(R.id.recycler_view);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
