package com.yumaas.beautifysafely.user;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.beautifysafely.CustomDialogClass;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.OnItemClickListener;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.SalonDetailsFragment;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.User;
import com.yumaas.beautifysafely.volleyutils.ConnectionHelper;

import java.util.ArrayList;

public class UserSalonsAdapter extends RecyclerView.Adapter<UserSalonsAdapter.ViewHolder> {

    OnItemClickListener onItemClickListener;

    ArrayList<User> users;
    Context context;

    public UserSalonsAdapter(Context context , OnItemClickListener onItemClickListener, ArrayList<User> users) {
        this.onItemClickListener=onItemClickListener;
        this.users = users;
        this.context=context;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }


    @Override
    public UserSalonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_barber_makup, parent, false);
        UserSalonsAdapter.ViewHolder viewHolder = new UserSalonsAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserSalonsAdapter.ViewHolder holder, final int position) {

        ConnectionHelper.loadImage(holder.imageView,users.get(position).image);
        holder.name.setText(users.get(position).name);
        holder.phone.setText(users.get(position).phone);
        holder.address.setText(users.get(position).address);
        holder.delete.setVisibility(View.GONE);

        try {
            CustomDialogClass alertDialog= new CustomDialogClass((Activity) holder.itemView.getContext());

            holder.delete.setOnClickListener(view -> SweetDialogs.twoButtonDialog(alertDialog,
                    "حذف المدربة"
                    , "هل متاكد من حذف المدربة ؟!",
                    "نعم",
                    "خروج", view1 -> {
                        alertDialog.cancel();
                        alertDialog.dismiss();
                        DataBaseHelper.removeUser(users.get(position));
                        users.remove(position);
                        notifyDataSetChanged();
                        SweetDialogs.singleButtonMessage(holder.itemView.getContext(), "تم المسح بنجاح");
                    }));
        }catch (Exception e){
            e.getStackTrace();
        }

        holder.itemView.setOnClickListener(view -> {
            Bundle bundle= new Bundle();
            UserSalonDetailsFragment salonDetailsFragment =  new UserSalonDetailsFragment();
            bundle.putSerializable("salon",users.get(position));
            salonDetailsFragment.setArguments(bundle);

            FragmentHelper.addFragment(context,salonDetailsFragment,"SalonDetailsFragment");
        });

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView,delete;
        TextView name,phone,address;
        public ViewHolder(View view) {
            super(view);
            imageView = view.findViewById(R.id.imageView);
            name= view.findViewById(R.id.name);
            phone= view.findViewById(R.id.phone);
            address= view.findViewById(R.id.address);
            delete=view.findViewById(R.id.delete);

        }
    }
}