package com.yumaas.beautifysafely.user;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.Service;
import com.yumaas.beautifysafely.models.TimeItem;
import com.yumaas.beautifysafely.models.User;

import java.util.ArrayList;

public class BookedTimesAdapter extends RecyclerView.Adapter<BookedTimesAdapter.ViewHolder> {


    ArrayList<Booked>timeItems;
    Context context;

    public BookedTimesAdapter(Context context , ArrayList<Booked>timeItems) {
        this.context=context;
        this.timeItems=timeItems;

    }


    @Override
    public int getItemCount() {
        return timeItems==null?0:timeItems.size();
    }


    @Override
    public BookedTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time
                , parent, false);
        BookedTimesAdapter.ViewHolder viewHolder = new BookedTimesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final BookedTimesAdapter.ViewHolder holder, final int position) {

        User user = DataBaseHelper.findUser(timeItems.get(position).salonId);
        holder.time.setText(" معاد الحجز : "+timeItems.get(position).timeItem.time+"  \n\n "+" اسم الصالون  :  "+
                user.name+"  \n\n "+" رقم الهاتف  : "+
                user.phone+"  \n\n "+" الخدمات المختارة  : "+timeItems.get(position).services);


    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        public ViewHolder(View view) {
            super(view);
        time = view.findViewById(R.id.tv_review_reviewer);

        }
    }






}