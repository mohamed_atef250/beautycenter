package com.yumaas.beautifysafely.user;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.abdeveloper.library.MultiSelectDialog;
import com.abdeveloper.library.MultiSelectModel;
import com.yumaas.beautifysafely.LoginActivity;
import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.RegisterActivity;
import com.yumaas.beautifysafely.RegisterSalonActivity;
import com.yumaas.beautifysafely.SweetDialogs;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Book;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.Service;
import com.yumaas.beautifysafely.models.TimeItem;
import com.yumaas.beautifysafely.models.User;

import java.util.ArrayList;

public class UserTimesAdapter extends RecyclerView.Adapter<UserTimesAdapter.ViewHolder> {


    ArrayList<TimeItem>timeItems;
    Context context;
    User user;
    public UserTimesAdapter(Context context , ArrayList<TimeItem>timeItems , User user) {
        this.context=context;
        this.timeItems=timeItems;
        this.user=user;
    }


    @Override
    public int getItemCount() {
        return timeItems==null?0:timeItems.size();
    }


    @Override
    public UserTimesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time
                , parent, false);
        UserTimesAdapter.ViewHolder viewHolder = new UserTimesAdapter.ViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final UserTimesAdapter.ViewHolder holder, final int position) {
        holder.time.setText(timeItems.get(position).time);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectServices(timeItems.get(position));
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView time;
        public ViewHolder(View view) {
            super(view);
        time = view.findViewById(R.id.tv_review_reviewer);

        }
    }


    private void selectServices(TimeItem timeItem){


        ArrayList<MultiSelectModel>multiSelectModels= new ArrayList<>();

        final ArrayList<Service>services = user.services;

        for(int i=0; i<services.size(); i++){
            multiSelectModels.add(new MultiSelectModel(services.get(i).id,services.get(i).title+"  "+services.get(i).price+"  ريال "));
        }


        MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .title("اختر الخدمات التي تريد ان تحجزها")
                .titleSize(25)
                .positiveText("تم")
                .negativeText("خروج")
                .multiSelectList(multiSelectModels)
                .setMinSelectionLimit(1)
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
             @Override
           public void onSelected(ArrayList<Integer> arrayList, ArrayList<String> arrayList1, String s) {
                int total=0;
                String selectedServices = "";
                for(int i=0; i<services.size(); i++){

                    if(arrayList.contains(services.get(i).id)){
                        try {
                            total += Integer.parseInt(services.get(i).price);
                            selectedServices+=" , "+services.get(i).title;
                        }catch (Exception e){
                            e.getStackTrace();
                        }
                    }
                }
                showTotalDialog(total,timeItem,selectedServices);
           }
           @Override
            public void onCancel() {

                              }
                          });


      multiSelectDialog.show(((FragmentActivity)context).getSupportFragmentManager(), "multiSelectDialog");

    }


    private void showTotalDialog(int total,TimeItem timeItem,String selectedServices){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(" السعر النهائي : "+total+"  ريال سعودي  ");
        builder1.setMessage("من فضللك اختر طريقه الدفع لاتمام الحجز");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "فيزا",
                (dialog, id) -> {
                    Booked booked = new Booked(DataBaseHelper.getSavedUser().id,user.id,timeItem);
                    booked.services = selectedServices;
                    booked.total=total;
                    DataBaseHelper.addBooked(booked);
                    SweetDialogs.singleButtonMessage(context,"تم الحجز بنجاح");
                    dialog.cancel();
                });

        builder1.setNegativeButton(
                "كاش",
                (dialog, id) -> {
                    Booked booked = new Booked(DataBaseHelper.getSavedUser().id,user.id,timeItem);
                    booked.total=total;
                    booked.services = selectedServices;
                    DataBaseHelper.addBooked(booked);
                    SweetDialogs.singleButtonMessage(context,"تم الحجز بنجاح");
                    dialog.cancel();
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}