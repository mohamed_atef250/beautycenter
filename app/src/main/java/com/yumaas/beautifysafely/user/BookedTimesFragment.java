package com.yumaas.beautifysafely.user;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.beautifysafely.R;
import com.yumaas.beautifysafely.base.DataBaseHelper;
import com.yumaas.beautifysafely.models.Book;
import com.yumaas.beautifysafely.models.Booked;
import com.yumaas.beautifysafely.models.User;

import java.util.ArrayList;


public class BookedTimesFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_times, container, false);
        rootView.findViewById(R.id.button2).setVisibility(View.GONE);

        ArrayList<Booked> bookeds = new ArrayList<>();
        ArrayList<Booked> bookedsTemp = DataBaseHelper.getDataLists().bookeds;

        User user = DataBaseHelper.getSavedUser();

        for(int i=0; i<bookedsTemp.size(); i++){
            if(bookedsTemp.get(i).userId==user.id){

                bookeds.add(bookedsTemp.get(i));
            }
        }

        BookedTimesAdapter chatAdapter = new BookedTimesAdapter(getActivity(),bookeds);

        menuList = rootView.findViewById(R.id.recycler_view);
        menuList.setLayoutManager(new LinearLayoutManager(getActivity()));
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
