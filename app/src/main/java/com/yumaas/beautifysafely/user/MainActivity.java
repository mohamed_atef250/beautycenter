package com.yumaas.beautifysafely.user;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.abedalkareem.tabview.AMTabView;
import com.yumaas.beautifysafely.FragmentHelper;
import com.yumaas.beautifysafely.R;

import java.util.ArrayList;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;


public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    private PopupMenu popUp = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       
        AMTabView bottomNavigation = findViewById(R.id.bottom_navigation);


        imageView= findViewById(R.id.language);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> genderList = new ArrayList<>();


                genderList.add("اللغه العربيه");
                genderList.add("English");


                if (popUp == null) {
                    popUp = new PopupMenu(MainActivity.this, imageView);
                    for (int i = 0; i < genderList.size(); i++) {
                        popUp.getMenu().add(i, i, i, genderList.get(i));
                    }


                    popUp.setOnMenuItemClickListener(item -> {

                        return false;
                    });
                }
                popUp.show();
                popUp = null;
            }
        });


        FragmentHelper.addFragment(this,new FragmentHome(),"FragmentHome");

        List<Integer> icons = new ArrayList<>();

        icons.add(R.drawable.ic_home);
        icons.add(R.drawable.ic_chat);
        icons.add(R.drawable.ic_search);
        icons.add(R.drawable.ic_times);

        bottomNavigation.setTabsImages(icons);


        bottomNavigation.setOnTabChangeListener(new Function1<Integer, Unit>() {
            @Override
            public Unit invoke(Integer integer) {
                if(integer==0){
                    FragmentHelper.addFragment(MainActivity.this,new FragmentHome(),"FragmentHome");
                }else  if(integer==1){
                    FragmentHelper.addFragment(MainActivity.this,new ChattersFragment(),"ChattersFragment");
                }else  if(integer==2){
                    FragmentHelper.addFragment(MainActivity.this,new SearchFragment(),"SearchFragment");
                }else  if(integer==3){
                    FragmentHelper.addFragment(MainActivity.this,new BookedTimesFragment(),"BookedTimesFragment");
                }
                return null;
            }
        });
       


    }
}