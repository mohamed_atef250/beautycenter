package com.yumaas.beautifysafely;

import android.app.Activity;
import android.content.Context;
import android.view.View;


public class SweetDialogs {

    public static void successMessage(Context context, String message, View.OnClickListener onSweetClickListener) {

        CustomDialogClass alertDialog= new CustomDialogClass((Activity) context);
        alertDialog.show();
        alertDialog.setTitle("تمت بنجاح");
        alertDialog.setMessage(message);



        alertDialog.setButton( "غلق", onSweetClickListener);

        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

    }

    public static void successMessage(CustomDialogClass alertDialog, String message, View.OnClickListener onSweetClickListener) {


        alertDialog.show();
        alertDialog.setTitle("تمت بنجاح");
        alertDialog.setMessage(message);



        alertDialog.setButton( "غلق", onSweetClickListener);

        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

    }

    public static void singleButtonMessage(Context context, String message) {

        CustomDialogClass alertDialog= new CustomDialogClass((Activity) context);
        alertDialog.show();
        alertDialog.setTitle("رساله");
        alertDialog.setMessage(message);
         alertDialog.setButton("غلق", view -> {
             alertDialog.cancel();
             alertDialog.dismiss();
         });

                 alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);


    }





    public static void twoButtonDialog(CustomDialogClass alertDialog, String title, String message, String confirmText, String cancelText, View.OnClickListener onConfirmListener) {


        alertDialog.show();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(confirmText,onConfirmListener);
        alertDialog.setButton2(cancelText, view -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);



    }



}
